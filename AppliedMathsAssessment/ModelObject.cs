﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace AppliedMathsAssessment
{
    class ModelObject : PhysicsObject
    {
        // ------------------
        // Data
        // ------------------

        // Rendering
        private Matrix[] transforms;
        private Model mesh;
        private GraphicsDevice graphicsDevice;
        private VertexBuffer cubeVertexBuffer;
        private BasicEffect hitBoxEffect;
        protected bool visible = false;
        protected float alpha = 1;
        protected bool drawHitBox = true;


        // ------------------
        // Behaviour
        // ------------------
        public void LoadModel(ContentManager content, string modelName, GraphicsDevice newGraphics)
        {
            mesh = content.Load<Model>(modelName);
            transforms = new Matrix[mesh.Bones.Count];
            mesh.CopyAbsoluteBoneTransformsTo(transforms);
            visible = true;

            // Setup for bounding box rendering
            graphicsDevice = newGraphics;
            int[] boxpos = new int[] { 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1,
0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0,
0, 1, 0 };
            VertexPositionColor[] boxVerts = new VertexPositionColor[17];
            for (int index = 0; index < boxpos.Length; index += 3)
                boxVerts[index / 3] = new VertexPositionColor(new
                Vector3(boxpos[index], boxpos[index + 1], boxpos[index + 2]), Color.White);
            cubeVertexBuffer = new VertexBuffer(graphicsDevice,
            typeof(VertexPositionColor), boxVerts.Length, BufferUsage.None);
            cubeVertexBuffer.SetData(boxVerts);
            hitBoxEffect = new BasicEffect(graphicsDevice);
        }
        // ------------------
        public void LoadModel(Model model)
        {
            mesh = model;
            transforms = new Matrix[mesh.Bones.Count];
            mesh.CopyAbsoluteBoneTransformsTo(transforms);
            visible = true;
        }
        // ------------------
        public void CopyTo(ModelObject newObject)
        {
            newObject.mesh = mesh;
            newObject.transforms = transforms;
            newObject.graphicsDevice = graphicsDevice;
            newObject.cubeVertexBuffer = cubeVertexBuffer;
            newObject.hitBoxEffect = hitBoxEffect;
            newObject.visible = visible;
            newObject.alpha = alpha;
            newObject.drawHitBox = drawHitBox;
        }
        // ------------------
        public void SetAlpha(float newAlpha)
        {
            alpha = newAlpha;
        }
        // ------------------
        public override void Draw(Camera cam, DirectionalLightSource light)
        {
            if (!visible) return; // dont render hidden meshes
            
            foreach (ModelMesh mesh in mesh.Meshes) // loop through the mesh in the 3d model, drawing each one in turn.
            {
                foreach (BasicEffect effect in mesh.Effects) // This loop then goes through every effect in each mesh.
                {
                    effect.World = transforms[mesh.ParentBone.Index]; // begin dealing with transforms to render the object into the game world
                                                                      // The following effects allow the object to be drawn in the correct place, with the correct rotation and scale.

                    ///////////////////////////////////////////////////////////////////
                    //
                    // CODE FOR TASK 1 SHOULD BE ENTERED HERE
                    //
                    ///////////////////////////////////////////////////////////////////  

                    effect.World = transforms[mesh.ParentBone.Index];

                    effect.World *= Matrix.CreateScale(scale);

                    //rotation
                    effect.World *= Matrix.CreateRotationX(rotation.X);
                    effect.World *= Matrix.CreateRotationY(rotation.Y);
                    effect.World *= Matrix.CreateRotationZ(rotation.Z);

                    effect.World *= Matrix.CreateTranslation(position);


                    //view
                    effect.View = Matrix.CreateLookAt(
                        cam.position, //position
                        cam.target,//target
                        Vector3.Up);// direction of up

                    //perspective
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(
                        cam.fieldOfView,
                        cam.aspectRatio,
                        cam.nearPlane,
                        cam.farPlane);

                    ///////////////////////////////////////////////////////////////////  
                    // END TASK 1 CODE
                    ///////////////////////////////////////////////////////////////////  

                    // the following effects are related to lighting and texture  settings, feel free to tweak them to see what happens.
                    effect.LightingEnabled = true;
                    effect.Alpha = alpha; //  amount of transparency
                    effect.AmbientLightColor = new Vector3(0.25f); // fills in dark areas with a small amount of light
                    effect.DiffuseColor = new Vector3(0.1f);
                    // Diffuse is the standard colour method
                    effect.DirectionalLight0.Enabled = true; // allows a directional light
                    effect.DirectionalLight0.DiffuseColor = light.diffuseColor; // the directional light's main colour
                    effect.DirectionalLight0.SpecularColor = light.specularColor; // the directional light's colour used for highlights
                    effect.DirectionalLight0.Direction = light.direction; // the direction of the light
                    effect.EmissiveColor = new Vector3(0.15f);
                }
                mesh.Draw(); // draw the current mesh using the effects.
            }

            if (drawHitBox)
                DrawBoundingBox(cam);
        }
        // ------------------
        public void DrawBoundingBox(Camera cam)
        {
            hitBoxEffect.LightingEnabled = false;
            hitBoxEffect.VertexColorEnabled = false;
            BoundingBox box = GetHitBox();
            Color wireColour = Color.Black; // TODO - change to white when colliding


            graphicsDevice.SetVertexBuffer(cubeVertexBuffer);
            hitBoxEffect.World =
            Matrix.CreateScale(box.Max - box.Min) *
            Matrix.CreateTranslation(box.Min);
            hitBoxEffect.View = Matrix.CreateLookAt(cam.position, cam.target, cam.whichWayIsUp);
            hitBoxEffect.Projection = Matrix.CreatePerspectiveFieldOfView(
                        cam.fieldOfView, cam.aspectRatio, cam.nearPlane, cam.farPlane);
            hitBoxEffect.DiffuseColor = wireColour.ToVector3();
            foreach (EffectPass pass in hitBoxEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphicsDevice.DrawPrimitives(PrimitiveType.LineStrip, 0, 16);
            }
        }
        // ------------------
        public override void UpdateHitBox()
        {
            ///////////////////////////////////////////////////////////////////
            //
            // CODE FOR TASK 2 SHOULD BE ENTERED HERE
            //
            ///////////////////////////////////////////////////////////////////

            // Axis aligned bounding box
             hitBox = new BoundingBox();

            foreach (ModelMesh mesh in mesh.Meshes) // loop through the mesh in the 3d model, drawing each one in turn.
            {
                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                {
                    // Create an array to store the vertex data.
                    VertexPositionNormalTexture[] modelVertices = new VertexPositionNormalTexture[meshPart.VertexBuffer.VertexCount];

                    // Get the model's vertices
                    meshPart.VertexBuffer.GetData(modelVertices);

                    // Create a new array to store the position of each vertex.
                    Vector3[] vertices = new Vector3[modelVertices.Length];

                    // Get the bone transform
                    Matrix meshTransform = mesh.ParentBone.Transform;
                    meshTransform *= Matrix.CreateScale(scale); // scale  the mesh to the right size
                    meshTransform *= Matrix.CreateScale(collisionScale); // scale the mesh by the collision scale
                    meshTransform *= Matrix.CreateRotationX(rotation.X); // rotate the mesh
                    meshTransform *= Matrix.CreateRotationY(rotation.Y); // rotate the mesh
                    meshTransform *= Matrix.CreateRotationZ(rotation.Z); // rotate the mesh

                    // Loop throught the vertices.
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        // Get the position of the vertex
                        // Transform it using the bone transform
                        vertices[i] = Vector3.Transform(modelVertices[i].Position, meshTransform);
                    }

                    // Create a AABB from the model's vertices
                    hitBox = BoundingBox.CreateMerged(hitBox, BoundingBox.CreateFromPoints(vertices));
                }
            }

            // Move our box to the correct place in the world
            hitBox.Min += position + collisionScale + mesh.Meshes[0].BoundingSphere.Center;
            hitBox.Max += position + collisionScale + mesh.Meshes[0].BoundingSphere.Center;

            

            ///////////////////////////////////////////////////////////////////  
            // END TASK 2 CODE
            ///////////////////////////////////////////////////////////////////
        }
        // ------------------
    }
}
